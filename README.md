## UM-ECE Computer Help Site

This is a website.  This repo holds the sourcecode.  The website is rendered with 'Hugo'.

The website is automatically rendered and hosted by Netlify at https://um-ece-help.netlify.app/

There should be a CNAME dns entry in the olemiss.edu server which directs requests bound to
a custom subdomain (e.g. "help.ece.olemiss.edu") to the netlify host above.

You can install Hugo locally and have it render this page with the commands:

```bash
sudo apt install hugo
cd <repository-path>
hugo serve
```
