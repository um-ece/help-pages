---
title: "Home"
date: 2020-10-23T21:55:48-05:00
draft: false
---

# Welcome to the UM ECE Help Pages

This site is a resource for the entire UM ECE community, faculty and students alike.
Please share and contribute any useful information to help other users of these tools.

## To get help:
[**Submit an Issue via Gitlab.com**](https://gitlab.com/um-ece/help-pages/-/issues/new)


## To contribute to this site:
See the "Learn" website documentation for some cool features of this website (https://learn.netlify.app/en/shortcodes/mermaid/)

Pages can be edited and added in several ways (**NOTE: Changes will not be reflected immediately; they have to be approved and merged in**):

 1. There is an "edit" link in the top right which will take you to the corresponding file in the site's gitlab repository.
    You can use the gitlab.com interface to make edits. You can commit your edits to a new branch and then create a "pull request"
    to merge your branch into the public website.

 2. There is an admin interface at [/admin]({{% siteparam baseURL %}}/admin). Anyone can register and use this interface.

 3. You can clone the entire website from the gitlab repository (making a local repo), commit your changes locally,
    then push your changes to a new branch in the gitlab.com repository (remote). You can then create a "pull request" to
    merge your branch into the public website.

