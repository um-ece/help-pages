---
title: "Virtuoso"
date: 2020-10-24T11:26:49-05:00
draft: false
---

Virtuoso is Cadence's graphical environment for managing circuit netlists (schematics) and their hierarchical
integration as complex systems.
