---
title: "Incisive"
date: 2020-10-24T11:26:49-05:00
draft: false
chapter: false
---

Cadence Incisive is used to create a netlist from a circuit layout which includes parasitic capacitances and resistances.
