---
title: "Terminals"
date: 2020-10-24T17:56:53-05:00
draft: false
---

Terminals are applications that provide a way to interact with a "shell" -- which is a
text-based program known as a REPL (read- evaluate- print-loop). The default Linux shell is "Bash".


The "shell" will print characters to STDOUT,STDERR and will read characters from STDIN.
It's the terminal application that connects your keyboard to STDIN and puts characters from STDOUT/STERR
onto your monitor display.

There are many options for terminals that offer different features:

## Linux:
 - xterm
 - Tilix
 - Terminology

## Mac
 - iTerm
 - terminal (built-in. found in utilities)

## Windows
 - DOS shell ('cmd' in start menu; )
 - Windows PowerShell (understands some common *nix/POSIX commands)
