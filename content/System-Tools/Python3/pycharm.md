+++
Title="PyCharm"
weight=99
date="2020-10-20T060200Z"
chapter=false
+++

Pycharm is a python IDE by JetBrains.  It is very powerful and very useful. It includes integration with "git" version control.

Download pycharm-community-edition [HERE](https://www.jetbrains.com/pycharm/download/#section=linux)
(*instead of the CE version, you can apply for an educational license to use the "Pro" version at no cost*)

## When you use PyCharm:
 - be sure to configure the "project interpreter" for each project. Pycharm
   integrates with Anaconda, so it's easy to create Anaconda environments
   and to configure PyCharm to use them for your project.
