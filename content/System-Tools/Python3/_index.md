+++
title = "Python 3"
date = 2020-10-23T21:14:30-05:00
weight = 5
chapter = false
# pre = "<b>X. </b>"
+++

### Python 3

Python is an interpreted language rather than a compiled one. This means that your program remains a text file
which is read by another running program called an interpreter. The interpreter follows your instructions and
generates machine code on the fly, which your CPU executes.

Before you can do anything, you have to make sure you have a Python interpreter.

** Python 2.x is now deprecated, meaning it is no longer officially supported, and not recommended for new designs.**

iPython is *an* interpreter.  There are many implementations of Python interpreter, but iPython is the dominant one.

## Windows:
To install a Python interpreter into your windows system: [Official Python Site](https://www.python.org/downloads/windows/)

*If you install [WSL]({{% relref "/System-tools/wsl2" %}}) with Ubuntu, the Ubuntu container will include `python3`*

## Mac:
Python 3 on Mac:
(link)

## Linux:
Most Linux distributions include a python interpreter. Ubuntu 20.04 includes `python3` by default which defaults
to version 3.8.5.  If you want a 2.x version, you can do `apt install python`.

Here are all the Python3 pages:
{{% children %}}
