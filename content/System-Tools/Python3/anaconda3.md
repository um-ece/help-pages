+++
title = "Anaconda Env Manager"
date = 2020-10-23T21:14:30-05:00
weight = 5
chapter = false
# pre = "<b>X. </b>"
+++

# Anaconda3 Package Manager

Like `apt` and `apt-get` for your Debian/Ubuntu Linux system, there are tools to manage the installation/removal
of packages for other sub-systems.

Anaconda3 is one a package manager for your Python3 system.
It is also able to manage several "virtual" python3 systems, each with a unique combination of packages available to them.

[Pip3]() is another package manager for Python3, but some tools install easier via Anaconda
(Google's TensorFlow and Facebook's PyTorch for example).

## Install:
 - Download and install **the Individual Edition from: https://www.anaconda.com/products/individual**   

## Configure
 - run `conda init --all` to setup your shell(s) for using `conda` commands.
 - run `conda config --set auto_activate_base false` to prevent anaconda from automatically putting you into the
"base" virtual env every time you use a Bash shell.

## Use:
 - run `conda create --name my-new-fancy-env` will create a new virtual python3 environment
 - `conda activate my-new-fancy-env` will put your current interactive shell "inside" our new virtual python system
 - `conda install numpy` to install the numpy package into the currently active conda environment.
 - `conda deactivate` will take you out of the virtual env and back to the outer shell.
