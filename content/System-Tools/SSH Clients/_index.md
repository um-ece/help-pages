---
title: "SSH Clients (Secure Shell)"
date: 2020-10-24T17:56:53-05:00
draft: false
---

SSH is an open protocol for encrypted communication between two entities over a network.
The program runing on the far end (not your computer) runs an SSH "server" which listens on a port (typically 22).
When you want to connect, you launch an SSH "client" on your local machine and provide the
ip address (or hostname) and port of the remote machine.

This is how you can log-in to a remote server and use a text console (terminal).

## Linux:
- openssh-client (install using `apt install openssh-client`)

## Mac
 - openssh-client (comes with OS X)

## Windows
 - openssh (comes with Win 10-2018 update) (https://fossbytes.com/enable-built-windows-10-openssh-client/)
 - [PuTTY]("https://www.chiark.greenend.org.uk/~sgtatham/putty/") (also includes SSH client and 'xterm' [terminal emulator]({{% ref "../Terminals/" %}}))
 - [KiTTY]("http://www.9bis.net/kitty/?#!index.md")
 - [MobaXterm]("https://mobaxterm.mobatek.net/")
