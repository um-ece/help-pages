+++
title = "Windows Subsystem for Linux"
date = 2020-10-23T21:14:30-05:00
weight = 5
chapter = true
# pre = "<b>X. </b>"
+++
# WSL: Windows Subsystem for Linux (v.1 / v.2)
# (a.k.a. run Ubuntu inside Win 10)

## How to setup:
 1. Before you do anything, you need to [ENABLE WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10#step-2---update-to-wsl-2)
 2. Be sure you've [updated to WSL2](https://docs.microsoft.com/en-us/windows/wsl/wsl2-kernel)
 3. Install a Linux Distro (Ubuntu 20.04) [link to Microsoft Store download](https://ubuntu.com/wsl)

## How to use:
 - Click "Start Menu" and start typing "Ubuntu"... click Ubuntu once it appears. You will find yourself at a Bash prompt.
