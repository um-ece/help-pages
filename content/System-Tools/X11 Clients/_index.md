---
title: "X11"
date: 2020-10-24T17:56:53-05:00
draft: false
---

X11 is the main graphics rendering system in the linux universe. One day it may be replaced
by 'Wayland' -- that's the name of the system that is meant to be its replacement.

Ninety-nine percent of graphical applications in linux need to communicate with an X11 
system in order to render windows.

In X11, the client/server relationship is a little unintuitive:
your local machine has access to your graphics hardware (GPU+Monitor), and so it acts as a 'server',
allowing 'clients' to connect and make use of your screen.
So, when you connect to a remote system, you will need an X11 'server' which will allow 
connections from 'client' applications running on the remote system.

This will allow you to run graphical
applications on a remote machine, and have the interface (window and buttons, etc.) render
on the computer in front of you.


## Linux:
 - X11  (often called xorg) is included in most linux distros.

## Mac
 - Xquartz  you can install it from [here](https://www.xquartz.org/index.html)

## Windows
 - [MobaXterm](https://mobaxterm.mobatek.net/) includes a terminal, SSH client, and X11 server
 - [Xming](http://www.straightrunning.com/XmingNotes/)
 - [VcXsrv](https://sourceforge.net/projects/vcxsrv/)