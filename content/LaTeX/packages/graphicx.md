+++
Title="graphicx"
+++

This package is useful for including images in all the modern formats (jpeg,gif,png,svg,pdf, etc.).
