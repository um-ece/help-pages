+++
Title="amsmath"
+++

Amsmath is a package that makes many common math notation symbols available for use in LaTeX.
