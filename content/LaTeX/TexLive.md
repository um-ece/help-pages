+++
Title= "TexLive Distribution"
chapter= false
date = "2020-10-29T055300Z"
weight = 01
+++

The Texlive distribution is a bundle of executables which are required to turn a file containing LaTeX code
into a publishable postsript or PDF file.

## Windows 10:
 - get a bundle (like TexLive) called ["MikTex"](https://miktex.org/download)

## Mac OS X:
 - Install ["MacTeX"](https://www.tug.org/mactex/)

## Debian/Ubuntu:
 - you can just install using apt: `apt install texlive-full`
